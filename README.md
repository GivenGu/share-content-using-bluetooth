# 用蓝牙共享内容

### 介绍

通过蓝牙而非网络共享文字内容。

逐步搭建原型的同时技术调研，详情见 [悬赏任务](https://gitee.com/zhishi/share-content-using-bluetooth/issues)。

进展：

1. [在两个win 10系统之间通过蓝牙传输文字信息](https://gitee.com/zhishi/share-content-using-bluetooth/issues/IABP3R)

### 参考

- [Briar](https://briarproject.org/)

